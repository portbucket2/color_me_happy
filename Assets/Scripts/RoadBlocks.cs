﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RoadBlocks : RoadController
{
    [Header("Color Shader")]
    [SerializeField] private Material matInside;
    [SerializeField] private float fillAmount;
    [SerializeField] private float fillingTime = 3f;
    [SerializeField] private float horizontalScale;
    //public Color colorOld;
    //public Color colorNew;
    private bool fillStart;

    [Header("Block Animation")]
    [SerializeField] bool scalable;
    [Range(0f, 5f)] [SerializeField] float scaleDuration = 1f;
    float halfScaleDuration;
    [Range(0.5f, 2f)] [SerializeField] float horizontalScaleMultiplayer = 1.2f;
    [Range(0.1f, 1f)] [SerializeField] float verticalScaleMultiplayer = 0.8f;

    [SerializeField] bool blockJump;
    [Range(0f, 5f)] [SerializeField] float jumpPower = 2f;

    //[SerializeField] MeshRenderer rend;
    //[SerializeField] Material eachMat;
    //[SerializeField] float fade = 1f;
    [SerializeField] bool isColorful = false;
    [SerializeField] bool isAnimated = false;

    Material mat;

    readonly string PLAYER = "Player";

    void Start()
    {
       // rend = this.GetComponent<MeshRenderer>();
       // eachMat = rend.material;

        halfScaleDuration = 0.5f * scaleDuration;
        Score.Instance.Attendence(1);

        //Shawon Bhai

        Material mat = GetComponent<Renderer>().material;
        matInside = new Material(mat);
        GetComponent<Renderer>().material = matInside;
        //matInside.SetColor("colorOld", colorOld);
        //matInside.SetColor("colorNew", colorNew);
        matInside.SetFloat("scale", 0.1f);

        //Shawon Bhai
    }

    void Update()
    {
        if (fillStart)
        {
            fillAmount += Time.deltaTime * (1 / fillingTime);
            matInside.SetFloat("colorFill", fillAmount);
            if (fillAmount >= 1)
            {
                fillAmount = 1;
                fillStart = false;
            }
        }
    }

    public void FillIt(Transform ball)
    {
        Vector3 localPos = transform.InverseTransformPoint(ball.position);
        if (horizontalScale == 0)
        {
            return;
        }
        float frac = localPos.x * 20f / horizontalScale;

        float c = frac;

        matInside.SetFloat("colorFill", 0f);
        matInside.SetFloat("center", c);
        fillStart = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER) && !isColorful)
        {
            FillIt(other.transform);
            //StartCoroutine(ColorLerp());
            isColorful = true;
            Debug.LogError("hit block");
            Score.Instance.ColorProgress(1);
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(PLAYER) && !isAnimated)
        {
            //GetComponent<GroundBar>()?.FillIt(other.transform);
            Colourize();
        }
    }
    void Colourize()
    {
        //fade = 0;
        isAnimated = true;
        StartCoroutine(ColourSequence());
        //magicCircle.transform.position = transform.position;
        //magicCircle.Play();

    }

    /*IEnumerator ColorLerp()
    {
        float timer = 0f;

        if (colorMat.name[0] == 'C')
        {
           // rend.material = colorMat;
        }
        else
        {
            while (timer <= scaleDuration)
            {
                timer += Time.deltaTime;
                float lerpTime = timer / scaleDuration;
               // rend.material.Lerp(eachMat, colorMat, lerpTime);
                yield return null;
            }
        }
    }*/

    IEnumerator ColourSequence()
    {
        if (scalable)
        {
            Vector3 initScale = transform.localScale;
            Vector3 endScale = transform.localScale * horizontalScaleMultiplayer;
            endScale.y = transform.localScale.y * verticalScaleMultiplayer;
            Vector3 smallScale = transform.localScale * verticalScaleMultiplayer;

            DOTween.Sequence()
            .Append(transform.DOScale(endScale, halfScaleDuration))
            .Append(transform.DOScale(initScale, halfScaleDuration))
            .SetAutoKill(true);
            ;
        }

        if (blockJump)
        {
            Vector3 initPos = transform.position;

            transform.DOJump(initPos, 0.75f * jumpPower, 1, 0.5f * scaleDuration);
            //DOTween.Sequence()
            //.Append(transform.DORotate(new Vector3(-30f, 0f, 0f), 0.5f * scaleDuration))
            //.Append(transform.DORotate(new Vector3(0f, 0f, 0f), 0.1f * scaleDuration));
            
        }

        yield return null;
    }
}
