﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField] private AnimationCurve jumpCurve;

    public void DoJump(float height, float duration)
    {
        StartCoroutine(JumpRoutine(height, duration));
    }

    public void DoJumpAndRoll(float height, Vector3 direction, float duration)
    {
        StartCoroutine(JumpAndRollRoutine(height, direction, duration));
    }


    IEnumerator JumpRoutine(float height, float duration)
    {
        float timeElapsed = 0f;
        Vector3 initPos = transform.localPosition;
        Vector3 currentPosition = transform.localPosition;

        while (timeElapsed < 1f)
        {
            currentPosition.y = height * jumpCurve.Evaluate(timeElapsed);
            transform.localPosition = currentPosition;
            timeElapsed += (Time.deltaTime / duration);
            yield return null;
        }
        transform.localPosition = initPos;
    }

    IEnumerator JumpAndRollRoutine(float height, Vector3 direction, float duration)
    {
        float timeElapsed = 0f;
        Vector3 currentPosition = transform.position;
        Vector3 initPosition = transform.position;

        while (timeElapsed <= 1f)
        {
            currentPosition += direction * (1f - timeElapsed) * Time.deltaTime;
            currentPosition.y = initPosition.y + height * jumpCurve.Evaluate(timeElapsed);
            transform.position = currentPosition;
            timeElapsed += (Time.deltaTime / duration);
            yield return null;
        }
    }
}
