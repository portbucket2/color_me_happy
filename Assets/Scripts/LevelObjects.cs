﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelObjects : MonoBehaviour
{
    [SerializeField] Material colorMat;
    [SerializeField] ParticleSystem magicCircle;

    [Header("Block Animation")]
    [SerializeField] bool scalable;
    [Range(0f, 5f)] [SerializeField] float scaleDuration = 1f;
    float halfScaleDuration;
    [Range(0.5f, 2f)] [SerializeField] float horizontalScaleMultiplayer = 1.2f;
    [Range(0.1f, 1f)] [SerializeField] float verticalScaleMultiplayer = 0.8f;

    [SerializeField] bool blockJump;
    [Range(0f, 5f)] [SerializeField] float jumpPower = 2f;

    [SerializeField] MeshRenderer rend;
    [SerializeField] Material eachMat;
    [SerializeField] float fade = 1f;
    [SerializeField] bool isColorful = false;
    [SerializeField] bool isAnimated = false;

    [SerializeField] private GameObject[] tinyEffect;

    [SerializeField] private bool doShake;

    readonly string PLAYER = "Player";

    void Start()
    {
        rend = this.GetComponent<MeshRenderer>();
        if (rend != null)
        {
            eachMat = rend?.material;
        }

        halfScaleDuration = 0.5f * scaleDuration;

        Score.Instance.Attendence(1);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER) && !isColorful)
        {
            StartCoroutine(Colorize());
            isColorful = true;
            Debug.LogError("hit block");
            Score.Instance.ColorProgress(1);
        }

        if (other.gameObject.CompareTag(PLAYER) && !isAnimated)
        {
            isAnimated = true;
            Animate();
        }
    }
    void Animate()
    {
        fade = 0;
        StartCoroutine(ColourSequence());
        //magicCircle.transform.position = transform.position;
        //magicCircle.Play();

    }

    IEnumerator Colorize()
    {
        yield return new WaitForEndOfFrame();
        if (rend != null)
        {
            rend.material = colorMat;
        }
        else
        {
            if(tinyEffect.Length == 1)
            {
                Debug.Log("Sign");
                tinyEffect[0].SetActive(true);
            }
            else if (tinyEffect.Length > 1)
            {
                for (int i = 0; i < tinyEffect.Length; i++)
                {
                    tinyEffect[i].GetComponent<MeshRenderer>().material = colorMat;
                }
                for (int i = 2; i < tinyEffect.Length; i++)
                {
                    tinyEffect[i].GetComponent<Dustbin>().enabled = true;
                }
                yield return new WaitForSeconds(1.2f);
                tinyEffect[0].transform.DOLocalRotate(new Vector3(105f, 0f, 0f), 0.35f);
            }
        }
        if(doShake)
        {
            float shakeTimer = 0f;
            GetComponent<Jump>()?.DoJump(0.007f, 2f);
            while (shakeTimer <= 1f)
            {
                transform.localRotation = Quaternion.Euler(0f, 360f * shakeTimer, 0f);
                shakeTimer += Time.deltaTime;
                yield return null;
            }
            scalable = true;
            blockJump = true;
        }
    }

    IEnumerator ColourSequence()
    {
        if (scalable)
        {
            Vector3 initScale = transform.localScale;
            Vector3 endScale = transform.localScale * horizontalScaleMultiplayer;
            endScale.y = transform.localScale.y * verticalScaleMultiplayer;
            Vector3 smallScale = transform.localScale * verticalScaleMultiplayer;

            DOTween.Sequence()
            .Append(transform.DOScale(endScale, halfScaleDuration))
            .Append(transform.DOScale(initScale, halfScaleDuration))
            .SetAutoKill(true);
            ;
        }

        if (blockJump)
        {
            Vector3 initPos = transform.position;

            transform.DOJump(initPos, 0.75f * jumpPower, 1, 0.5f * scaleDuration);
            //DOTween.Sequence()
            //.Append(transform.DORotate(new Vector3(-30f, 0f, 0f), 0.5f * scaleDuration))
            //.Append(transform.DORotate(new Vector3(0f, 0f, 0f), 0.1f * scaleDuration));

        }

        isAnimated = false;
        yield return null;
    }
}
