﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ball Data", menuName = "Ball Data")]
public class BallData : ScriptableObject
{
    public enum touchType { mouse, joystick}
    public touchType currentTouchType;

    public float ballSpeed;
    public float ballDrag;
    public float ballImpact;
}
