﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] bool isInputEnabled = true;
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] Transform cam;
    [SerializeField] VariableJoystick joystick;
    [SerializeField] Transform cameraPos;

    
    float horizontalInput;
    float verticalInput;
    Vector3 direction;
    Vector3 mouseStart;
    InteractableObjects currentBuilding;

    readonly string BUILDINGS = "buildings";

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseStart = Input.mousePosition;
        }
        if (isInputEnabled && Input.GetMouseButton(0))
        {
            horizontalInput = joystick.Horizontal * moveSpeed;
            verticalInput = joystick.Vertical * moveSpeed;

            direction = Input.mousePosition - mouseStart;
            direction = new Vector3(direction.x, 0f, direction.y);
            //Debug.DrawLine(transform.position, direction * 2f, Color.red);

            Vector3 camF = cam.forward;
            Vector3 camR = cam.right;

            camF.y = 0;
            camR.y = 0;
            camF = camF.normalized;
            camR = camR.normalized;

            Vector3 dd = (camF * verticalInput + camR * horizontalInput) * Time.deltaTime * 5f;
            //Debug.DrawLine(transform.position, dd * 3f, Color.blue);
            transform.position += dd;
            
        }

        if (isInputEnabled)
        {
            Vector3 dir = transform.position - cameraPos.position;
            Ray ray = new Ray(cameraPos.position, dir);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit,200))
            {
                Debug.DrawLine(ray.origin, hit.point,Color.green);
                if (hit.transform.CompareTag(BUILDINGS))
                {
                    if (hit.transform.GetComponent<InteractableObjects>())
                    {
                        currentBuilding = hit.transform.GetComponent<InteractableObjects>();
                        currentBuilding.Transparent();
                    }
                }
                else
                {
                    if (currentBuilding)
                    {
                        currentBuilding.Opaque();
                        currentBuilding = null;
                    }
                }
            }
        }
    }
}
