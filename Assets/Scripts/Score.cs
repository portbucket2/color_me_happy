﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
public class Score : MonoBehaviour
{
    [SerializeField] private Image barFiller;
    [SerializeField] private Color[] barColor;
    [SerializeField] private float lerpSpeed = 2f;
    [SerializeField] private Button nextButton;
    [SerializeField] private GameObject[] signText;
    [SerializeField] private GameObject mark60;
    [SerializeField] private GameObject mark80;
    [SerializeField] private GameObject mark100;
    [SerializeField] private Text levelCounter;
    public int objectToColor = 0;
    public int doneColoring = 0;

    private bool mark60crossed = false;
    private bool mark80crossed = false;
    private bool mark100crossed = false;

    public bool eventSent = false;


    public static Score Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        LevelManager.Instance.currentLevel++;
        LionStudios.Analytics.Events.LevelStarted(LevelManager.Instance.currentLevel);
        levelCounter.text = "Lvl " + LevelManager.Instance.currentLevel.ToString();
    }
    void Update()
    {
        UpdateBar();
    }

    private void UpdateBar()
    {
        barFiller.fillAmount = Mathf.Lerp(barFiller.fillAmount, (float)doneColoring / (float)objectToColor, Time.deltaTime * lerpSpeed);

        if(doneColoring == objectToColor && barFiller.fillAmount > 0.9f)
        {
            mark100.GetComponentInChildren<Text>().color = Color.yellow;
            mark100.transform.GetChild(0).GetComponent<Outline>().enabled = true;
            if (!mark100crossed)
            {
                LionStudios.Analytics.Events.LevelComplete(LevelManager.Instance.currentLevel);
                eventSent = true;

                mark100crossed = true;
                signText[2].SetActive(true);

                DOTween.Sequence()
                    .Append(mark100.transform.DOScale(Vector3.one * 1.5f, 0.5f))
                    .Append(mark100.transform.DOScale(Vector3.one, 0.5f));

            }
        }
        else if(barFiller.fillAmount > 0.8f)
        {
            mark80.GetComponentInChildren<Text>().color = Color.yellow;
            mark80.transform.GetChild(0).GetComponent<Outline>().enabled = true;

            if (!mark80crossed)
            {
                mark80crossed = true;
                signText[1].SetActive(true);
                barFiller.color = barColor[1];

                DOTween.Sequence()
                    .Append(mark80.transform.DOScale(Vector3.one * 1.5f, 0.5f))
                    .Append(mark80.transform.DOScale(Vector3.one, 0.5f));
            }
        }
        else if(barFiller.fillAmount > 0.6f)
        {
            mark60.GetComponentInChildren<Text>().color = Color.yellow;
            mark60.transform.GetChild(0).GetComponent<Outline>().enabled = true;

            nextButton.gameObject.SetActive(true);
            nextButton.gameObject.transform.DOScale(Vector3.one, 2f);
            if (!mark60crossed)
            {
                mark60crossed = true;
                signText[0].SetActive(true);
                barFiller.color = barColor[0];

                DOTween.Sequence()
                    .Append(mark60.transform.DOScale(Vector3.one * 1.5f, 0.5f))
                    .Append(mark60.transform.DOScale(Vector3.one, 0.5f));

            }
        }
    }

    public void Attendence(int value)
    {
        objectToColor += value;
    }

    public void ColorProgress(int value)
    {
        doneColoring += value;
    }
}
