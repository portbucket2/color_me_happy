﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Building : MonoBehaviour
{
    [SerializeField] Material curnish;
    [SerializeField] Material glass;
    [SerializeField] Material walls;
    [SerializeField] Material orange;

    [SerializeField] ParticleSystem magicCircle;
    //[SerializeField] List<Material> eachMaterials;

    [Header("Building Animation")]
    [Range(0f, 5f)] [SerializeField] float colorTransactionTime = 1f;
    [Range(0f, 5f)] [SerializeField] float colorLerpTime = 1f;
    private float eachColorDelay;

    [SerializeField] bool scalable;
    [Range(0f, 5f)] [SerializeField] float scaleDuration = 1f;
    [Range(0.5f, 2f)] [SerializeField] float horizontalScaleMultiplayer = 1.2f;
    [Range(0.1f, 1f)] [SerializeField] float verticalScaleMultiplayer = 0.8f;
    private float halfScaleDuration;
    private float eachLayerColorTT;


    [SerializeField] bool blockJump;
    [Range(0f, 5f)] [SerializeField] float jumpPower = 2f;


    [SerializeField] bool rotateable;
    [Range(0f, 5f)] [SerializeField] float rotationDuration = 1f;
    [Range(0f, 360f)] [SerializeField] float rotationAngle = 90f;
    private bool rotationCompleted = true;
    private float streachedTime;
    private float eachRotationDelay;

    [SerializeField] float fade = 1f;
    [SerializeField] bool isColorFul = false;
    private bool colorCompleted = true;

    private List<List<MeshRenderer>> layers;
    private List<List<Material>> objMats;

    readonly string FADE = "_fade";
    readonly string PLAYER = "Player";
    readonly string Buildings = "buildings";
    readonly string BASECOLOR = "_BaseColor";
    int layerCount;

    [SerializeField] private GameObject citizen;


    void Start()
    {
        halfScaleDuration = scaleDuration * 0.5f;
        layers = new List<List<MeshRenderer>>();
        objMats = new List<List<Material>>();

        layerCount = transform.childCount;

        for (int i = 0; i < layerCount; i++)
        {
            Transform thisChild = transform.GetChild(i);

            layers.Add(new List<MeshRenderer>());
            objMats.Add(new List<Material>());

            if (thisChild.childCount > 0)
            {
                MeshRenderer childRenderer = thisChild.GetComponent<MeshRenderer>();
                layers[i].Add(childRenderer);
                objMats[i].Add(childRenderer.material);

                foreach(Transform child in thisChild)
                {
                    MeshRenderer secondChildRenderer = child.GetComponent<MeshRenderer>();
                    layers[i].Add(secondChildRenderer);
                    objMats[i].Add(secondChildRenderer.material);
                }
            }
            else
            {
                MeshRenderer childRenderer = thisChild.GetComponent<MeshRenderer>();
                layers[i].Add(childRenderer);
                objMats[i].Add(childRenderer.material);
            }
        }

        streachedTime = 1f / rotationDuration;
        eachRotationDelay = 0.5f * rotationDuration / layerCount;
        eachColorDelay = 0.5f * colorTransactionTime / layerCount;
        eachLayerColorTT = colorTransactionTime / layerCount;

        Score.Instance.Attendence(layerCount);
    }

    private float rotationTimer = 0f;
    private float colorTimer = 0f;
    private float rot;
    private int colorLayerFrom = 0;
    private int colorLayerTo = 0;

    private bool isAnimated = false;

    private void Update()
    {
        SequentialColorWithLerp();

        SequentialRotation();
    }

    /*void Start()
    {
        rends = new List<MeshRenderer>();
        eachMats = new List<Material>();
        //eachMaterials = new List<Material>();
        childCount = transform.childCount;
        if (childCount > 0)
        {

            for (int i = 0; i < childCount; i++)
            {
                rends.Add(transform.GetChild(i).GetComponent<MeshRenderer>());
                eachMats.Add(rends[i].material);
                //eachMaterials.Add(mats[i].color);
            }
            for (int i = 0; i < childCount; i++)
            {
                rends[i].material = bwMat;
            }

        }
        else
        {
            rends.Add(GetComponent<MeshRenderer>());
            eachMats.Add(rends[0].material);
        }

    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            if (isColorFul)
            {
                if (!isAnimated)
                {
                    isAnimated = true;
                    StartCoroutine(SequentialScaleJump());
                }
            }
            else
            {
                Colourize();
                StartCoroutine(ActiveCitizen());
                Score.Instance.ColorProgress(layerCount);
            }

            Debug.Log("building");
        }
    }
    void Colourize()
    {
        fade = 0;
        isColorFul = true;
        Debug.LogError("hit building");
        rotationCompleted = false;
        colorCompleted = false;
        if (magicCircle != null)
        {
            //magicCircle.transform.position = transform.position;
            magicCircle.Play();
        }
    }

    IEnumerator SequentialScaleJump()
    {
        for (int i = 0; i < layerCount; i++)
        {
            for (int j = 0; j < layers[i].Count; j++)
            {
                Vector3 initScale = layers[i][j].transform.localScale;
                Vector3 endScale = layers[i][j].transform.localScale * horizontalScaleMultiplayer;
                endScale.y = layers[i][j].transform.localScale.y * verticalScaleMultiplayer;

                DOTween.Sequence()
                .Append(layers[i][j].transform.DOScale(endScale, halfScaleDuration))
                .Append(layers[i][j].transform.DOScale(initScale, halfScaleDuration))
                .SetAutoKill(true);
                ;
                Vector3 initPos = layers[i][j].transform.position;

                layers[i][j].transform.DOJump(initPos, jumpPower, 1, scaleDuration);
            }

            isAnimated = false;
            yield return null;
        }
    }

    private void SequentialRotation()
    {
        if (!rotationCompleted)
        {
            rotationTimer += Time.deltaTime;

            if (rotationTimer <= rotationDuration * 2f)
            {
                for (int i = 0; i < layerCount; i++)
                {
                    rot = Mathf.Min((rotationTimer - i * eachRotationDelay) * streachedTime * rotationAngle, rotationAngle);
                    if (rot > 0f)
                    {
                        layers[i][0].transform.localRotation = Quaternion.Euler(0f, rot, 0f);
                    }
                }

            }
            else
            {
                rotationCompleted = true;
            }
        }
    }

    private bool scoreCounted = false;
    private void SequentialColorWithLerp()
    {
        if (!colorCompleted)
        {
            colorTimer += Time.deltaTime;

            if (colorTimer <= colorTransactionTime)
            {
                for (int i = 0; i < layerCount; i++)
                {
                    float colorLerp = Mathf.Min((colorTimer - i * eachColorDelay) * streachedTime, 1f);
                    if (colorLerp > 0f)
                    {
                        for (int j = 0; j < layers[i].Count; j++)
                        {
                            switch (layers[i][j].material.name[0])
                            {
                                case 'C':
                                    layers[i][j].material.Lerp(layers[i][j].material, curnish, colorLerp);
                                    break;
                                case 'G':
                                    layers[i][j].material.Lerp(layers[i][j].material, glass, colorLerp);
                                    break;
                                case 'W':
                                    layers[i][j].material.Lerp(layers[i][j].material, walls, colorLerp);
                                    break;
                                case 'O':
                                    layers[i][j].material.Lerp(layers[i][j].material, orange, colorLerp);
                                    break;
                                default:
                                    layers[i][j].material = layers[i][j].material;
                                    break;
                            }

                        }
                    }
                }

            }
            else
            {
                rotationCompleted = true;
            }
        }
    }

    IEnumerator ActiveCitizen()
    {
        yield return new WaitForSeconds(1f);
        citizen.SetActive(true);
    }

    private void SequentialColor()
    {
        if (!colorCompleted)
        {
            colorTimer += Time.deltaTime;

            if (colorTimer <= 2f * colorTransactionTime && colorLayerFrom < layerCount)
            {
                colorLayerTo = Mathf.CeilToInt(colorTimer / eachLayerColorTT);
                for (int i = colorLayerFrom; i <= colorLayerTo; i++)
                {
                    for (int j = 0; j < layers[i].Count; j++)
                    {
                        switch (layers[i][j].material.name[0])
                        {
                            case 'C':
                                layers[i][j].material.Lerp(layers[i][j].material, curnish, Mathf.PingPong(Time.time, 1f));
                                break;
                            case 'G':
                                layers[i][j].material.Lerp(layers[i][j].material, glass, Mathf.PingPong(Time.time, 1f));
                                break;
                            case 'W':
                                layers[i][j].material.Lerp(layers[i][j].material, walls, Mathf.PingPong(Time.time, 1f));
                                break;
                            case 'O':
                                layers[i][j].material.Lerp(layers[i][j].material, orange, Mathf.PingPong(Time.time, 1f));
                                break;
                            default:
                                layers[i][j].material = layers[i][j].material;
                                break;
                        }

                    }
                }

                colorLayerFrom = colorLayerTo + 1;
            }
            else
            {
                colorCompleted = true;
            }
        }
    }


    /*IEnumerator ColourSequence()
    {
        for (int i = 0; i < childCount; i++)
        {
            rends[i].material = eachMats[i];
            Vector3 scale = new Vector3(rends[i].transform.localScale.x, rends[i].transform.localScale.y, rends[i].transform.localScale.z);
            //Vector3 scale = new Vector3(0f,180f,0f);
            //Debug.Log("scale1: " + scale);
            scale -= scale / 1000f;
            //Debug.Log("scale2: " + scale);
            rends[i].transform.DOPunchScale(scale, 0.5f, 2, 1f).SetEase(Ease.InSine);
            //rends[i].transform.DOShakeScale(0.1f, 0.5f, 1).SetEase(Ease.InSine);
            yield return EndOfFrame;
        }
    }*/
    public void Transparent()
    {
        //rends.material = transMat;
        //mats = rends.material;
        //mats.DOFloat(fade, FADE, 0.1f);
    }
    public void Opaque()
    {
        //rends.material = opaqueMat;
        //mats = rends.material;
        //mats.DOFloat(fade, FADE, 0.1f);
    }
}
