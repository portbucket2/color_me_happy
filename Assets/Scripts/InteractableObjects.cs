﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableObjects : MonoBehaviour
{
    [SerializeField] Material opaqueMat;
    [SerializeField] Material transMat;
    [SerializeField] Material bwMat;
    [SerializeField] ParticleSystem magicCircle;
    //[SerializeField] List<Material> eachMaterials;
    [SerializeField] Color bwColor;

    [Header("Building Animation")]
    [SerializeField] bool scalable;
    [SerializeField] bool doubleWave;
    [Range(0f, 5f)] [SerializeField] float scaleDuration = 1f;
    float halfScaleDuration;
    [Range(0.5f, 2f)] [SerializeField] float horizontalScaleMultiplayer = 1.2f;
    [Range(0.1f, 1f)] [SerializeField] float verticalScaleMultiplayer = 0.8f;

    [SerializeField] bool blockJump;
    [Range(0f, 5f)] [SerializeField] float jumpPower = 2f;


    [SerializeField] bool rotateable;
    [Range(0f, 5f)] [SerializeField] float rotationDuration = 1f;
    [Range(0f, 360f)] [SerializeField] float rotationAngle = 90f;
    private bool rotationCompleted = true;
    private float streachedTime;
    private float rotDelay;

    [Header("For Debug: Auto fill")]
    [SerializeField] List<MeshRenderer> rends;
    [SerializeField] List<Material> eachMats;
    [SerializeField] float fade = 1f;
    [SerializeField] bool isColorFul = false;

    private List<List<MeshRenderer>> layers;
    private List<List<Material>> objMats;
    private List<char> layerKey;

    readonly string FADE = "_fade";
    readonly string PLAYER = "Player";
    readonly string Buildings = "buildings";
    readonly string BASECOLOR = "_BaseColor";
    int childCount = 0;
    int layerCount = -1;
    WaitForSeconds WAIT = new WaitForSeconds(0.02f);
    WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();


    void Start()
    {
        halfScaleDuration = scaleDuration * 0.5f;
        layers = new List<List<MeshRenderer>>();
        objMats = new List<List<Material>>();
        layerKey = new List<char>();
        //eachMaterials = new List<Material>();
        childCount = transform.childCount;

        for (int i = 0; i < childCount; i++)
        {
            char firstChar = transform.GetChild(i).name[0];
            MeshRenderer childRenderer = transform.GetChild(i).GetComponent<MeshRenderer>();

            if (layerKey.Contains(firstChar))
            {
                layers[layerCount].Add(childRenderer);
                objMats[layerCount].Add(childRenderer.material);
            }
            else
            {
                layerCount++;
                layers.Add(new List<MeshRenderer>());
                layers[layerCount].Add(childRenderer);

                objMats.Add(new List<Material>());
                objMats[layerCount].Add(childRenderer.material);
                layerKey.Add(firstChar);
            }
        }
        foreach (var layer in layers)
        {
            foreach (var l in layer)
            {
                l.material = bwMat;
            }
        }

        streachedTime = 1f / rotationDuration;
        rotDelay = 0.5f * rotationDuration / layerCount;
    }

    private float timer = 0f;
    private float colorTimer = 0f;
    private float rot;
    private void Update()
    {
        if (!rotationCompleted)
        {
            timer += Time.deltaTime;
            colorTimer += Time.deltaTime;

            if (timer <= rotationDuration * 2f)
            {
                for (int i = 0; i <= layerCount; i++)
                {
                    rot = Mathf.Min((timer - i * rotDelay) * streachedTime * rotationAngle, rotationAngle);
                    if (rot > 0f)
                    {
                        for (int j = 0; j < layers[i].Count; j++)
                        {
                            layers[i][j].transform.rotation = Quaternion.Euler(0f, rot, 0f);
                            layers[i][j].material = objMats[i][j];
                        }
                    }
                }

            }
            else
            {
                rotationCompleted = true;
            }
        }
    }

    /*void Start()
    {
        rends = new List<MeshRenderer>();
        eachMats = new List<Material>();
        //eachMaterials = new List<Material>();
        childCount = transform.childCount;
        if (childCount > 0)
        {

            for (int i = 0; i < childCount; i++)
            {
                rends.Add(transform.GetChild(i).GetComponent<MeshRenderer>());
                eachMats.Add(rends[i].material);
                //eachMaterials.Add(mats[i].color);
            }
            for (int i = 0; i < childCount; i++)
            {
                rends[i].material = bwMat;
            }

        }
        else
        {
            rends.Add(GetComponent<MeshRenderer>());
            eachMats.Add(rends[0].material);
        }

    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER) && !isColorFul)
        {
            Colourize();
        }
    }
    void Colourize()
    {
        fade = 0;
        isColorFul = true;
        Debug.LogError("hit building");
        //mats.DOFloat(fade, FADE, 3f);
        if (childCount > 0)
        {
            StartCoroutine(ColourSequence());
            magicCircle.transform.position = transform.position;
            magicCircle.Play();
        }


        //magicCircle.transform.position = transform.position;


        //magicCircle.Play();
        //Vector3 scale = new Vector3(transform.localScale.x, transform.localScale.y * 1.3f, transform.localScale.z);
        //transform.DOPunchScale(scale , 1).SetEase(Ease.Linear);
    }

    IEnumerator ColourSequence()
    {
        for (int i = 0; i <= layerCount; i++)
        {
            for (int j = 0; j < layers[i].Count; j++)
            {
                layers[i][j].material = objMats[i][j];

                if (scalable)
                {
                    Vector3 initScale = layers[i][j].transform.localScale;
                    Vector3 endScale = layers[i][j].transform.localScale * horizontalScaleMultiplayer;
                    endScale.y = layers[i][j].transform.localScale.y * verticalScaleMultiplayer;

                    if (doubleWave)
                    {
                        DOTween.Sequence()
                        .Append(layers[i][j].transform.DOScale(endScale, 0.5f * halfScaleDuration))
                        .Append(layers[i][j].transform.DOScale(initScale, 0.5f * halfScaleDuration))
                        .Append(layers[i][j].transform.DOScale(endScale, 0.5f * halfScaleDuration))
                        .Append(layers[i][j].transform.DOScale(initScale, 0.5f * halfScaleDuration))
                        .SetAutoKill(true);
                        ;
                    }
                    else
                    {
                        DOTween.Sequence()
                        .Append(layers[i][j].transform.DOScale(endScale, halfScaleDuration))
                        .Append(layers[i][j].transform.DOScale(initScale, halfScaleDuration))
                        .SetAutoKill(true);
                        ;
                    }
                }

                if (blockJump)
                {
                    Vector3 initPos = layers[i][j].transform.position;

                    layers[i][j].transform.DOJump(initPos, jumpPower, 1, scaleDuration);
                }

                if (rotateable)
                {
                    rotationCompleted = false;
                }
            }

            //rends[i].transform.DOShakeScale(0.1f, 0.5f, 1).SetEase(Ease.InSine);
            yield return new WaitForSeconds(0.01f);
        }
    }

    /*IEnumerator ColourSequence()
    {
        for (int i = 0; i < childCount; i++)
        {
            rends[i].material = eachMats[i];
            Vector3 scale = new Vector3(rends[i].transform.localScale.x, rends[i].transform.localScale.y, rends[i].transform.localScale.z);
            //Vector3 scale = new Vector3(0f,180f,0f);
            //Debug.Log("scale1: " + scale);
            scale -= scale / 1000f;
            //Debug.Log("scale2: " + scale);
            rends[i].transform.DOPunchScale(scale, 0.5f, 2, 1f).SetEase(Ease.InSine);
            //rends[i].transform.DOShakeScale(0.1f, 0.5f, 1).SetEase(Ease.InSine);
            yield return EndOfFrame;
        }
    }*/
    public void Transparent()
    {
        //rends.material = transMat;
        //mats = rends.material;
        //mats.DOFloat(fade, FADE, 0.1f);
    }
    public void Opaque()
    {
        //rends.material = opaqueMat;
        //mats = rends.material;
        //mats.DOFloat(fade, FADE, 0.1f);
    }
}
