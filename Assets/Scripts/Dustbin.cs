﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Dustbin : MonoBehaviour
{
    [SerializeField] private float fallValue;
    [Range(0f, 5f)] [SerializeField] private float movementSpeed = 1f;

    private float nodeOffset = 0.0001f;
    private bool moveable = true;

    private Vector3 target;

    private Vector3 initPos;
    void Start()
    {
        
        transform.DOLocalJump(new Vector3(0f, -1f, 0f), 1.5f, 1, 2f);
    }
}
