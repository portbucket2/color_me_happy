﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class CamController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera vCam;
    [SerializeField] private Transform player;
    [SerializeField] private float maxPlayerPosition;
    [SerializeField] private float minPlayerPosition;

    float distance;
    float FOVPerDistance;
    
    void Start()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
        distance = maxPlayerPosition - minPlayerPosition;
    }
    void Update()
    {
        vCam.m_Lens.FieldOfView = Mathf.Clamp(60f - (player.position.z - minPlayerPosition) * 20f/distance, 40f, 60f);
    }
}
