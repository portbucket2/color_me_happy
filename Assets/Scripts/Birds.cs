﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Birds : MonoBehaviour
{
    [Range(0f, 50f)] [SerializeField] private float movementSpeed = 20f;

    [SerializeField] private Transform leftWing;
    [SerializeField] private Transform rightWing;

    [Range(0f, 60f)] [SerializeField] private float maxWingAngle = 30f;
    [Range(0f, 10f)] [SerializeField] private float wingSpeed = 1f;
    private float nodeOffset = 0.0001f;
    private bool moveable = true;
    private Vector3 targetDir;

    private Vector3 initPos;

    private float timer = 0f; 


    void Start()
    {
        initPos = transform.position;

        targetDir = GetRandomTarget();

        while(Physics.Raycast(transform.position, targetDir))
        {
            targetDir = GetRandomTarget();
        }

        float angleY = Mathf.Atan2(targetDir.z, targetDir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, -angleY, 0f);
    }


    void Update()
    {
        timer += Time.deltaTime;

        if (timer > 0.7f)
        {
            leftWing.transform.localRotation = Quaternion.Euler((0.125f - Mathf.PingPong(Time.time, 0.25f)) * 500f, 0f, 0f);
            rightWing.transform.localRotation = Quaternion.Euler((0.125f - Mathf.PingPong(Time.time, 0.25f)) * -500f, 0f, 0f);
            transform.position += targetDir * Time.deltaTime * movementSpeed * 0.5f * (timer);
        }
    }

    private Vector3 GetRandomTarget()
    {
        Vector3 tDir;
        tDir.x = Random.Range(-0.5f, 0.5f);
        if (tDir.x > 0f)
        {
            tDir.x += 0.5f;
        }
        else
        {
            tDir.x -= 0.5f;
        }

        tDir.z = Random.Range(-0.5f, 0.5f);
        if (tDir.z > 0f)
        {
            tDir.z += 0.5f;
        }
        else
        {
            tDir.z -= 0.5f;
        }

        tDir.y = 0.3f;

        return tDir;
    }
}
