﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Citizen : MonoBehaviour
{
    [SerializeField] private Vector3 target;
    [Range(0f, 10f)] [SerializeField] private float knockbackJumpPower = 5f;
    [Range(0f, 5f)] [SerializeField] private float movementSpeed = 1f;

    private float nodeOffset = 0.0001f;
    private bool moveable = true;

    private Vector3 initPos;

    private Animator anim;

    readonly string WALKING = "walking";
    readonly string CELEBRATION = "celebration";


    void Start()
    {
        anim = GetComponent<Animator>();
        anim.Play(WALKING);
        transform.LookAt(target);
        initPos = transform.position;
    }


    void Update()
    {
        if (moveable)
        {
            if ((target - transform.position).sqrMagnitude > nodeOffset)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, movementSpeed * Time.deltaTime);
            }
            else
            {
                anim.Play(CELEBRATION);
                target = initPos;
                moveable = false;
                StartCoroutine(MoveAgain());
            }
        }
    }

    IEnumerator MoveAgain()
    {
        
        yield return new WaitForSeconds(2f);
        transform.LookAt(initPos);
        anim.Play(WALKING);
        moveable = true;
    }
}
