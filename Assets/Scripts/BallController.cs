﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BallController : MonoBehaviour
{
    [SerializeField] private BallData ballData;
    [SerializeField] private bool isInputEnabled = true;
    [SerializeField] private float moveSpeed = 15f;
    [SerializeField] private float impactMultiplier = 3f;
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Transform cam;
    [SerializeField] private ParticleSystem splatParticle;




    #region TouchVariables

    private Vector3 mouseStart;
    private Vector3 mouseDelta;
    private Vector3 touchDir;
    private float touchOffset;
    private float horizontalInput;
    private float verticalInput;
    private bool isDragging;

    #endregion

    
    InteractableObjects currentBuilding;
    readonly string BUILDINGS = "buildings";

    private Animator anim;
    private Rigidbody rb;
    private Vector3 movementDir;
    private Vector3 abstractDir;
    private bool touched = false;

    private bool gameStarted = false;

    [SerializeField] private GameObject startText;
    [SerializeField] private GameObject infinityTouch;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        touchOffset = Screen.width * 0.1f;

        abstractDir = transform.position;
        anim = transform.GetChild(0).GetComponent<Animator>();

        UpdateDataOnStart();
    }

    private void UpdateDataOnStart()
    {
        moveSpeed = ballData.ballSpeed;
        rb.drag = ballData.ballDrag;
        impactMultiplier = ballData.ballImpact;
    }


    private void FixedUpdate()
    {
        if (gameStarted)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouseStart = Input.mousePosition;
                isDragging = true;
                startText.SetActive(false);
                infinityTouch.SetActive(false);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                mouseStart = Vector3.zero;
                isDragging = false;
            }

            if (Input.GetMouseButton(0) && isInputEnabled)
            {
                mouseDelta = (Input.mousePosition - mouseStart);

                touchDir.x = Mathf.Min(mouseDelta.x / touchOffset, 1f);
                touchDir.y = Mathf.Min(mouseDelta.y / touchOffset, 1f);

                horizontalInput = touchDir.x * moveSpeed;
                verticalInput = touchDir.y * moveSpeed;

                Vector3 camForward = new Vector3(cam.forward.x, 0f, cam.forward.z);
                Vector3 camRight = new Vector3(cam.right.x, 0f, cam.right.z);

                movementDir = (camForward.normalized * verticalInput + camRight.normalized * horizontalInput);

                rb.AddForce(movementDir, ForceMode.Impulse);

                if (rb.velocity.magnitude > moveSpeed)
                {
                    rb.velocity = rb.velocity.normalized * moveSpeed;
                }

                mouseStart = Input.mousePosition;
                touchDir = Vector3.zero;
            }
        }
        else
        {
            float distance = startPosition.z - transform.position.z;

            if (distance > 0.01f)
            {
                transform.position = Vector3.MoveTowards(transform.position, startPosition, (distance + 1f) * 0.1f * Time.fixedDeltaTime * moveSpeed);
                if(distance < 2f)
                {
                    if(!startText.activeInHierarchy)
                    {
                        infinityTouch.SetActive(true);
                        startText.SetActive(true);
                        startText.transform.DOScale(Vector3.one, 1f).OnComplete(SetTextAnimation);
                        
                    }

                    if(Input.GetMouseButtonDown(0))
                    {
                        gameStarted = true;
                        mouseStart = Input.mousePosition;
                        isDragging = true;
                        infinityTouch.SetActive(false);
                        startText.SetActive(false);
                    }
                }
            }
            else
            {
                gameStarted = true;
            }
        }
    }

    private void SetTextAnimation()
    {
        startText.GetComponent<Animator>().enabled = true;
    }

    private void LateUpdate()
    {
        if(Time.frameCount % 10 == 0)
        {
            abstractDir = transform.position - abstractDir;
        }
    }

    Vector3 targetObjDir;
    private void OnTriggerEnter(Collider other)
    {
        if (!touched)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Ground"))
            {
                touched = true;
                targetObjDir = (transform.position - other.transform.position);
                targetObjDir.y = 0;
                targetObjDir = targetObjDir.normalized;
                if (other.gameObject.tag != "Character")
                {
                    Vector3 splatDir = -targetObjDir;
                    splatParticle.transform.position = Vector3.up + transform.position;// + splatDir * 0.3f;
                    splatParticle.Stop();
                    splatParticle.Play();
                }
                StartCoroutine(TouchImpact());
            }
        }
    }

    IEnumerator TouchImpact()
    {
        //isInputEnabled = false;
        Vector3 ballInitPos = transform.position;
        yield return null;
        GetComponent<Collider>().enabled = false;
        yield return null;

        Vector3 dir = targetObjDir; // (ballInitPos - transform.position);

        dir.y = 0f;
        dir = dir.normalized;
        dir *= impactMultiplier;
        GetComponent<Jump>().DoJumpAndRoll(0.25f * impactMultiplier, dir, 1f);
        //isInputEnabled = true;

        yield return new WaitForSeconds(0.4f);
        anim.Play("squash");

        yield return new WaitForSeconds(0.7f);
        GetComponent<Collider>().enabled = true;
        touched = false;
    }
}
