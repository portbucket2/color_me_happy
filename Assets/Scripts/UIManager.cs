using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Button nextButton;

    private void Awake()
    {
        nextButton.onClick.AddListener(() =>
        {
            if (!Score.Instance.eventSent)
            {
                LionStudios.Analytics.Events.LevelComplete(LevelManager.Instance.currentLevel);
            }
            SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % 3);
        });
    }
}
