﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HumanAnimation : MonoBehaviour
{
    [SerializeField] private SkinnedMeshRenderer charSkin;
    [SerializeField] private Material charColorMat;
    [SerializeField] private GameObject faceDepressed;
    [SerializeField] private GameObject faceAmazed;
    [SerializeField] private GameObject facehappy;
    [SerializeField] private Vector3[] movementNode;
    [Range(0f, 10f)] [SerializeField] private float knockbackJumpPower = 5f;
    [Range(0f, 5f)] [SerializeField] private float movementSpeed = 1f;
    [SerializeField] private ParticleSystem particle;

    private Vector3 target;
    private int currentNode;
    private float nodeOffset = 0.0001f;
    private bool moveable = true;

    private Animator anim;
    private RuntimeAnimatorController ac;

    readonly string DEPRESSED = "depressed";
    readonly string WALKING = "walking";
    readonly  string KNOCKBACK = "knockback";
    readonly string CELEBRATION = "celebration";
    readonly string FREEZE = "freeze";
    readonly string PLAYER = "Player";

    private float celebrationTime;
    private float knockbackTime;

    private bool isColorFul = false;
    private enum animationPhase {Depressed, Walking, Knockback, Celebration, Freeze };


    void Start()
    {
        anim = GetComponent<Animator>();
        ac = anim.runtimeAnimatorController;

        foreach (AnimationClip clip in ac.animationClips)
        {
            switch (clip.name)
            {
                case "celebration":
                    celebrationTime = clip.length;
                    break;
                case "knockback":
                    knockbackTime = clip.length;
                    break;
            }
        }
        ChangeAnimationPhase(animationPhase.Depressed);

        currentNode = 0;
        target = movementNode[currentNode];
        transform.LookAt(target);

        Score.Instance.Attendence(1);
    }


    void Update()
    {
        if (moveable)
        {
            if ((target - transform.position).sqrMagnitude > nodeOffset)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, movementSpeed * Time.deltaTime);

                if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hitInfo, 0.5f))
                {
                    if (hitInfo.collider != null)
                    {
                        if (hitInfo.collider.tag == "Character")
                        {
                            ChangeNode();
                        }
                    }
                }
            }
            else
            {
                ChangeNode();
            }
        }
    }

    private void ChangeNode()
    {
        currentNode++;
        target = movementNode[currentNode % movementNode.Length];
        StartCoroutine(SmoothLookAt(target, 360f));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            if (!isColorFul)
            {
                isColorFul = true;
                StartCoroutine(Colorize());
                Score.Instance.ColorProgress(1);
            }
            else
            {
                StartCoroutine(JustJump());
            }
        }
    }

    private IEnumerator Colorize()
    {
        moveable = false;
        ChangeAnimationPhase(animationPhase.Knockback);
        MakeAmazedFace();
        charSkin.material = charColorMat;
        particle.Play();
        GetComponent<Jump>()?.DoJump(knockbackJumpPower, 0.75f * knockbackTime);
        Vector3 initScale = transform.localScale;
        Vector3 scale = transform.localScale * 1.5f;

        DOTween.Sequence()
            .Append(transform.DOScale(scale, 0.5f * celebrationTime))
            .Append(transform.DOScale(initScale, .5f * celebrationTime));

        StartCoroutine(SmoothLookAt(Camera.main.transform.position, 360f));

        yield return new WaitForSeconds(knockbackTime); ////////////////////////////////////////////////////////////////
        ChangeAnimationPhase(animationPhase.Celebration);
        MakeHappyFace();

        
        yield return new WaitForSeconds(celebrationTime); //////////////////////////////////////////////////////////////
        ChangeAnimationPhase(animationPhase.Walking);

        StartCoroutine(SmoothLookAt(target, 360f));

        movementSpeed *= 1.5f;
        moveable = true;
    }

    private IEnumerator JustJump()
    {
        moveable = false;
        ChangeAnimationPhase(animationPhase.Celebration);

        GetComponent<Jump>()?.DoJump(knockbackJumpPower, 0.75f * knockbackTime);
        Vector3 initScale = transform.localScale;
        Vector3 scale = transform.localScale * 1.5f;

        DOTween.Sequence()
            .Append(transform.DOScale(scale, 0.5f * celebrationTime))
            .Append(transform.DOScale(initScale, .5f * celebrationTime));

        StartCoroutine(SmoothLookAt(Camera.main.transform.position, 360f));

        yield return new WaitForSeconds(knockbackTime); ////////////////////////////////////////////////////////////////
        ChangeAnimationPhase(animationPhase.Walking);

        StartCoroutine(SmoothLookAt(target, 360f));

        moveable = true;
    }

    private void ChangeAnimationPhase(animationPhase phase)
    {
        switch (phase)
        {
            case animationPhase.Depressed:
                anim.Play(DEPRESSED);
                break;
            case animationPhase.Walking:
                anim.Play(WALKING);
                break;
            case animationPhase.Knockback:
                anim.Play(KNOCKBACK);
                break;
            case animationPhase.Celebration:
                anim.Play(CELEBRATION);
                break;
            case animationPhase.Freeze:
                anim.Play(FREEZE);
                break;
        }
    }

    private void MakeAmazedFace()
    {
        facehappy.SetActive(false);
        faceDepressed.SetActive(false);
        faceAmazed.SetActive(true);
    }
    private void MakeHappyFace()
    {
        faceDepressed.SetActive(false);
        faceAmazed.SetActive(false);
        facehappy.SetActive(true);
    }


    IEnumerator SmoothLookAt(Vector3 to, float speedInDegree)
    {
        Vector3 oldDirection = transform.forward;
        Vector3 newDirection = to - transform.position;

        float localTimer = 0f;

        float requiredTime = Vector3.Angle(transform.forward, newDirection) / speedInDegree;

        while (localTimer <= requiredTime)
        {
            transform.rotation = Quaternion.Lerp(Quaternion.LookRotation(oldDirection), Quaternion.LookRotation(newDirection), localTimer / requiredTime);
            localTimer += Time.deltaTime;
            yield return null;
        }
        transform.rotation = Quaternion.LookRotation(newDirection);
    }
}
