﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundColor : MonoBehaviour
{
    [SerializeField] bool resetOnStart = true;
    [SerializeField] int brushSize = 10;
    [SerializeField] Transform player;
    public Camera cam;
    public Color groundColor;

    readonly string GROUND = "groundTop";
    Vector2 centerPixel;

    void Start()
    {
        // cam = GetComponent<Camera>();
        if (resetOnStart)
            Reset();
    }

    void Update()
    {
        if (!Input.GetMouseButton(0))
            return;
        Ray ray = new Ray(player.position + Vector3.up, Vector3.down);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit)) 
            return;
        if (!hit.transform.CompareTag(GROUND))
            return;

        Debug.DrawLine(ray.origin, hit.point, Color.green,5f);
        Renderer rend = hit.transform.GetComponent<Renderer>();
        MeshCollider meshCollider = hit.collider as MeshCollider;

        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return;

        Texture2D tex = rend.material.mainTexture as Texture2D;
        Vector2 pixelUV = hit.textureCoord;
        pixelUV.x *= tex.width;
        pixelUV.y *= tex.height;

        centerPixel = new Vector2((int)pixelUV.x, (int)pixelUV.y);
        tex.SetPixel((int)centerPixel.x, (int)centerPixel.y, groundColor);
        tex.Apply();

        
        int cx = (int)centerPixel.x;
        int cy = (int)centerPixel.y;
        int r = brushSize;
        int r2 = r * r;
        int dx = 0;
        int dy = 0;
        for (dy = 0; dy < r; dy++)
        {
            dx = 0;
            while (dx * dx + dy * dy <= r2)
            {
                float dis = Vector2.Distance(new Vector2(cx, cy), new Vector2(cx + dx, cy + dy));
                //if (tex.GetPixel(cx + dx, cy + dy).a > 0.5f)
                //{
                //    groundColor.a = Mathf.InverseLerp(r / 2, r, dis);
                //}
                //else
                //{
                //    groundColor.a = 0f;
                //}
                //groundColor.a = Mathf.InverseLerp(r / 2, r, dis);


                if (tex.GetPixel(cx + dx, cy + dy).a > 0.1f)
                {
                    groundColor.a = Mathf.InverseLerp(r / 2, r, dis);
                }
                else
                {
                    groundColor.a = 0f;
                }
                tex.SetPixel(cx + dx, cy + dy, groundColor);

                if (tex.GetPixel(cx - dx, cy + dy).a > 0.1f)
                {
                    groundColor.a = Mathf.InverseLerp(r / 2, r, dis);
                }
                else
                {
                    groundColor.a = 0f;
                }
                tex.SetPixel(cx - dx, cy + dy, groundColor);

                if (tex.GetPixel(cx + dx, cy - dy).a > 0.1f)
                {
                    groundColor.a = Mathf.InverseLerp(r / 2, r, dis);
                }
                else
                {
                    groundColor.a = 0f;
                }
                tex.SetPixel(cx + dx, cy - dy, groundColor);

                if (tex.GetPixel(cx - dx, cy - dy).a > 0.1f)
                {
                    groundColor.a = Mathf.InverseLerp(r / 2, r, dis);
                }
                else
                {
                    groundColor.a = 0f;
                }
                tex.SetPixel(cx - dx, cy - dy, groundColor);


                dx++;
            }
        }
        tex.Apply();

        
    }

    public float ThetaScale = 0.01f;
    public float radius = 3f;
    private int Size;
    private float Theta = 0f;

    void FindAllPoints()
    {
        Theta = 0f;
        Size = (int)((1f / ThetaScale) + 1f);
        for (int i = 0; i < Size; i++)
        {
            Theta += (2.0f * Mathf.PI * ThetaScale);
            float x = radius * Mathf.Cos(Theta);
            float y = radius * Mathf.Sin(Theta);
        }

    }
    public void Reset()
    {
        Renderer rend = transform.GetComponent<Renderer>();
        Texture2D tex = rend.material.mainTexture as Texture2D;
        Color[] colorArray = new Color[tex.width * tex.height];

        for (int i = 0; i < colorArray.Length; i++)
        {
            colorArray[i] = Color.white;
            //colorArray[i].a = 0;

        }
        //maskedColorArray = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();
    }
}
